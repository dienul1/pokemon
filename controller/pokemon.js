var axios = require('axios').default
const moment= require('moment')
const host = 'https://pokeapi.co/'
const SQL = require("../knex/index")
class Pokemon {

    async GetInfoPokemon (req, res ) {
        try {
            console.log(req.body)

            const dataUser = {
                user_name : req.body.username,
                created_at:moment().format("YYYY-MM-DD HH:MM:ss")
            }
            const dataUserLog = {
                log_type : req.body.log_type,
                log_req :JSON.stringify(req.body),
                created_at:moment().format("YYYY-MM-DD HH:MM:ss")
            }

            const key = req.body.id
            const url = host + `/api/v2/pokemon/${key}/`
            const responseAPi = await axios({
                method: 'post',
                url: url,
              })
            const response = responseAPi['data'] 
            const {name , types, weight, height} = response
            const type = types[0].type.name
            
            const resultString = `${name} is an <${type}> type Pokemon with ${weight} and ${height} height, here's a picture of ${name}`
            await SQL.Create("user_log",dataUserLog)
            await SQL.Create("tb_user",dataUser)
            const resp={
                data: resultString,
                img: response.sprites.front_default
            }
            res.json(resp)
        } catch (error) {
            console.log("error GetInfoPokemon >>", error)
            res.status(500).json(error.message)
        }
    }
}

module.exports = new Pokemon()