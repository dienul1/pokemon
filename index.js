var express = require('express')
var cors = require('cors')
var app = express()

app.use(cors())

var path = require('path');
var dir = path.join("./uploads", 'public');

var fs = require('fs');


const bodyParser = require('body-parser')
const routes = require("./routes/index")

var mime = {
    html: 'text/html',
    txt: 'text/plain',
    css: 'text/css',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    js: 'application/javascript'
};
app.use(express.static(dir));

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded


//ROUTER
app.use('/',routes)


app.listen(2000)
