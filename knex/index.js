require('dotenv').config()
const knex = require('knex')({
    client: 'mysql',
    connection: {
        host: process.env.DB_HOST,
        user:  process.env.DB_UNAME,
        password: process.env.DB_PASS,
        database:  process.env.DB_NAME
    }
});


class Mysql {
    async Create(table, data) {
        try {
            console.log(process.env.DB_HOST)
            return await knex(table).insert(data)
         } catch(e) {
             throw new Error(e)
          }
    }


}

module.exports=new Mysql()