
var express = require('express');
var router = express.Router();

const pokemon = require('../controller/pokemon')

/* GET home page. */
router.get('/status', function(req, res, next) {
  res.send('SERVER KATA.AI IS ON');
});

router.post('/pokemon', pokemon.GetInfoPokemon)


module.exports = router;